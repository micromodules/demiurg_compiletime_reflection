#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

namespace CTR
{
    struct Const;
    struct NonConst;
}

namespace CTR
{
    struct Fail;
    struct Any;
    struct None;
}

#endif
