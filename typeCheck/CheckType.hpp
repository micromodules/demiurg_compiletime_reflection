#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "../implementation/Impl_CheckType.hpp"

namespace CTR
{
    template<class TCheckingTypeInfo, class TCheckRequest>
    using TypeCheckResult = CTR::Impl::TypeCheckResult::Impl<TCheckingTypeInfo, TCheckRequest>;
}

#endif
