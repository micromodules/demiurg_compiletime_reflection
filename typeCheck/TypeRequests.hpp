#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include <cstddef>//for std::size_t

namespace CTR
{
    //TODO: Check arguments correctness (because arguments may be filled by lib users)

    template<typename TConst, typename TType>
    struct PrimitiveTypeRequest;

    template<typename TConst, typename TType>
    struct EnumTypeRequest;

    template<typename TConst, typename TType>
    struct UnionTypeRequest;

    template<typename TConst, typename TType>
    struct ClassTypeRequest;

    template<typename TConst, typename TElementType, std::size_t TSize>
    struct ArrayTypeRequest;

    template<class TConst, class TElementType>
    struct UnsizedArrayTypeRequest;

    template<class TConst, class TMemberType, class TClassType>
    struct ClassPointerToMemberTypeRequest;

    template<class TConst, class TMethodConst, class TClassType, class TRet, class ... TArgs>
    struct ClassPointerToMethodTypeRequest;

    template<class TConst, class TRet, class ... TArgs>
    struct PointerToFunctionTypeRequest;

    template<class TConst, class TPointedType>
    struct PointerTypeRequest;

    template<class TPointedType>
    struct ReferenceTypeRequest;

    template<class TPassedType> struct PassedTypeCheckResult { using OkType = TPassedType; };
    template<class TFailedType> struct FailedTypeCheckResult { using FailType = TFailedType; };

}//namespace CTR

#endif
