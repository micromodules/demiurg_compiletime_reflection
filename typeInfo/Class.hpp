#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "Common.hpp"
#include "../implementation/Impl_TypeInfo_Class.hpp"

#include <type_traits>//for static_assert checks (should be removed?)

namespace CTR
{
    template<typename TConst, typename TClassType>
    struct ClassTypeInfo : public CommonTypeInfo<TConst>
    {
        static_assert(std::is_class<TClassType>::value, "TType should be class");

        using Type = CTR::ApplyConst<TConst, TClassType>;

        //Details

        //-Fields
        //TODO: Implement any type access
        template<class TMeta, class TFieldType, class ... TFieldSpecializationArgs>
        constexpr static auto getPointerToField();

        template<class TMeta, class TFieldType, class ... TFieldSpecializationArgs>
        constexpr static bool isFieldExist();

        template<class TMeta, class TFieldType, class ... TFieldSpecializationArgs>
        static TFieldType* getObjectFieldPointer(TClassType& inObject);

        template<class TMeta, class TFieldType, class ... TFieldSpecializationArgs>
        static const TFieldType* getObjectFieldPointer(const TClassType& inObject);

        //-Methods
        //TODO: Add here description of the optional arguments
        template<class TMeta, class TSignature, class ... TOptionalArgs>
        constexpr static auto getMethod();

        template<class TMeta, class TSignature, class ... TOptionalArgs>
        constexpr static bool isMethodExist();

        template<class TMeta, class ... TOptionalArgs>
        constexpr static bool isMethodMayBeCalled();

        //-Constructors & Destructors
        template<class ... TArgumentToPassTypes>
        constexpr static bool isConstructorMayBeCalled();

        constexpr static bool isDefaultConstructorMayBeCalled();

        constexpr static bool isDestructorMayBeCalled();

        struct Static
        {
            //-Static fields
            //TODO: Implement any type access

            template<class TMeta, class TFieldType, class ... TFieldSpecializationArgs>
            constexpr static TFieldType* getFieldPointer();

            template<class TMeta, class TFieldType, class ... TFieldSpecializationArgs>
            constexpr static bool isFieldExist();

            //-Static methods
            template<class TMeta, class TSignature, class ... TMethodSpecializationArgs>
            constexpr static CTR::PointerToFunction<TSignature> getMethodPointer();

            template<class TMeta, class TSignature, class ... TMethodSpecializationArgs>
            constexpr static bool isMethodExist();
        };
    };

}//namespace CTR

//============================================ Implementation ==============================================================================

namespace CTR
{
    //Details
    //-Fields

    template<class TConst, class TClassType>
    template<class TMeta, class TFieldType, class ... TFieldSpecializationArgs>
    constexpr auto ClassTypeInfo<TConst, TClassType>::getPointerToField(){
        using namespace CTR::Impl::ClassTypeInfoDetails;
        return GetPointerToField::_<TClassType, TMeta, TFieldType, TFieldSpecializationArgs...>();
    }

    template<typename TConst, typename TClassType>
    template<class TMeta, class TFieldType, class ... TFieldSpecializationArgs>
    constexpr bool ClassTypeInfo<TConst, TClassType>::isFieldExist() {
        return (getPointerToField<TMeta, TFieldType>() != nullptr);
    }

    template<typename TConst, typename TClassType>
    template<class TMeta, class TFieldType, class ... TFieldSpecializationArgs>
    TFieldType* ClassTypeInfo<TConst, TClassType>::getObjectFieldPointer(TClassType& inObject) {
        const TClassType& theConstObject = const_cast<const TClassType&>(inObject);
        const TFieldType* theConstFieldPointer = getObjectFieldPointer<TMeta, TFieldType, TFieldSpecializationArgs...>(theConstObject);
        return static_cast<TFieldType*>(theConstFieldPointer);
    }

    template<typename TConst, typename TClassType>
    template<class TMeta, class TFieldType, class ... TFieldSpecializationArgs>
    const TFieldType* ClassTypeInfo<TConst, TClassType>::getObjectFieldPointer(const TClassType& inObject) {
        using namespace CTR::Impl::ClassTypeInfoDetails;
        return GetObjectFieldPointer::_<TClassType, TMeta, TFieldType, TFieldSpecializationArgs...>(inObject);
    }

    //-Methods

    template<typename TConst, typename TClassType>
    template<class TMeta, class TSignature, class ... TOptionalArgs>
    constexpr auto ClassTypeInfo<TConst, TClassType>::getMethod() {
        using namespace CTR::Impl::ClassTypeInfoDetails;
        return GetMethod::Res<TConst, TClassType, TMeta, TSignature, TOptionalArgs...>::_();
    }

    //MethodRequestConst

    template<typename TConst, typename TClassType>
    template<class TMeta, class TSignature, class ... TOptionalArgs>
    constexpr bool ClassTypeInfo<TConst, TClassType>::isMethodExist() {
        return (getMethod<TMeta, TSignature, TOptionalArgs...>() != nullptr);
    }

    template<typename TConst, typename TClassType>
    template<class TMeta, class ... TOptionalArgs>
    constexpr bool ClassTypeInfo<TConst, TClassType>::isMethodMayBeCalled() {
        //TODO: Implement
        return false;
    }

    //-Constructors & Destructors

    template<typename TConst, typename TClassType>
    template<class ... TArgumentToPassTypes>
    constexpr bool ClassTypeInfo<TConst, TClassType>::isConstructorMayBeCalled() {
        using namespace CTR::Impl::ClassTypeInfoDetails;
        return IsConstructorMayBeCalled::_<TClassType, TArgumentToPassTypes...>();
    }

    template<typename TConst, typename TClassType>
    constexpr bool ClassTypeInfo<TConst, TClassType>::isDefaultConstructorMayBeCalled() {
        using namespace CTR::Impl::ClassTypeInfoDetails;
        return IsDefaultConstructorMayBeCalled::_<TClassType>();
    }

    template<typename TConst, typename TClassType>
    constexpr bool ClassTypeInfo<TConst, TClassType>::isDestructorMayBeCalled() {
        using namespace CTR::Impl::ClassTypeInfoDetails;
        return IsDestructorMayBeCalled::_<TClassType>();
    }

    //-Static fields

    template<typename TConst, typename TClassType>
    template<class TMeta, class TFieldType, class ... TFieldSpecializationArgs>
    constexpr TFieldType* ClassTypeInfo<TConst, TClassType>::Static::getFieldPointer() {
        //TODO: Implement
        return nullptr;
    }

    template<typename TConst, typename TClassType>
    template<class TMeta, class TFieldType, class ... TFieldSpecializationArgs>
    constexpr bool ClassTypeInfo<TConst, TClassType>::Static::isFieldExist() {
        return (getFieldPointer<TMeta, TFieldType, TFieldSpecializationArgs...>() != nullptr);
    }

    //-Static methods

    template<typename TConst, typename TClassType>
    template<class TMeta, class TSignature, class ... TMethodSpecializationArgs>
    constexpr CTR::PointerToFunction<TSignature> ClassTypeInfo<TConst, TClassType>::Static::getMethodPointer() {
        //TODO: Implement
        return nullptr;
    }

    template<typename TConst, typename TClassType>
    template<class TMeta, class TSignature, class ... TMethodSpecializationArgs>
    constexpr bool ClassTypeInfo<TConst, TClassType>::Static::isMethodExist() {
        return (isMethodExist<TMeta, TSignature, TMethodSpecializationArgs...>() != nullptr);
    }

}//namespace CTR

#endif
