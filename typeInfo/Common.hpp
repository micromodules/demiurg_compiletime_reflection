#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "../typeGen/ApplyConst.hpp"//for child TypeInfos' usage

namespace CTR
{
    template<typename TConst>
    struct CommonTypeInfo
    {
        using Const = TConst;
    };

}//namespace CTR

#endif
