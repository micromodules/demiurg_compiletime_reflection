#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "Common.hpp"

namespace CTR
{
    template<typename TPointedType>
    struct ReferenceTypeInfo
    {
        using PointedType = TPointedType;
        using Type = TPointedType&;
    };

}//namespace CTR

#endif
