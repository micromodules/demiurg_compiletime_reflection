#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "Common.hpp"

namespace CTR
{
    template<typename TConst, typename TPointedType>
    struct PointerTypeInfo : public CommonTypeInfo<TConst>
    {
        using PointedType = TPointedType;
        using Type = CTR::ApplyConst<TConst, TPointedType*>;
    };

}//namespace CTR

#endif
