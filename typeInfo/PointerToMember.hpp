#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "Common.hpp"
#include "../typeGen/Pointers.hpp"//for CTR::PointerToMember

namespace CTR
{
    template<typename TConst, class TMemberType, class TClassType>
    struct ClassPointerToMemberTypeInfo : public CommonTypeInfo<TConst>
    {
        using MemberType = TMemberType;
        using ClassType = TClassType;
        using Type = CTR::ApplyConst<TConst, CTR::PointerToMember<TMemberType, TClassType>>;
    };

}//namespace CTR

#endif
