#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "Common.hpp"

namespace CTR
{
    template<typename TConst, typename TType>
    struct EnumTypeInfo : public CommonTypeInfo<TConst>
    {
        using Type = CTR::ApplyConst<TConst, TType>;
    };

}//namespace CTR

#endif
