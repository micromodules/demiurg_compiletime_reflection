#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include <utility>//For std::declval

namespace CTR{ struct IdentifierMeta{ }; }

#define DDECL_MetaImpl_PointerToMember(MIdent)                                                                                             \
    template<class Class, class Type, class FailType, class TArg0, class ... TOtherArgs>                                                   \
    constexpr static auto getPointerToMember(void*)->decltype(static_cast<Type>(&Class::template MIdent<TArg0, TOtherArgs...>))            \
                                                                       { return &Class::template MIdent<TArg0, TOtherArgs...>; }           \
                                                                                                                                           \
    template<class Class, class Type, class FailType>                                                                                      \
    constexpr static auto getPointerToMember(void*)->decltype(static_cast<Type>(&Class::MIdent))                                           \
                                                                       { return &Class::MIdent; }                                          \
                                                                                                                                           \
    template<class Class, class Type, class FailType, class ... TUnusedArgs>                                                               \
    constexpr static FailType getPointerToMember(...) { return nullptr; }                                                                  \

#define DDECL_IdentifierMeta(MIdent)                                                                                                       \
struct MIdent : public CTR::IdentifierMeta                                                                                                 \
{                                                                                                                                          \
    DDECL_MetaImpl_PointerToMember(MIdent)                                                                                                 \
};

#define DDECL_OperatorMeta(MMetaName, MOperator)                                                                                           \
struct MMetaName : public CTR::IdentifierMeta                                                                                              \
{                                                                                                                                          \
    DDECL_MetaImpl_PointerToMember(operator MOperator)                                                                                     \
};

#endif
