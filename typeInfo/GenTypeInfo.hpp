#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "../implementation/Impl_TypeInfo.hpp"

namespace CTR
{
    template<class TType>
    using GenTypeInfo = typename CTR::Impl::TypeInfo::Impl<TType>::_;
}

#endif
