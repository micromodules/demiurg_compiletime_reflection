#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "Common.hpp"
#include "../typeGen/Pointers.hpp"//for CTR::PointerToMethod

namespace CTR
{
    template<typename TConst, class TClassType, class TMethodConst, class TRet, class ... TArgs>
    struct ClassPointerToMethodTypeInfo
    {
        using ClassType = TClassType;
        using MethodConst = NonConst;
        using Signature = TRet(TArgs...);
        using Type = CTR::ApplyConst<TConst, CTR::PointerToMethod<TClassType, TRet(TArgs ...), TMethodConst>>;
    };

}//namespace CTR

#endif
