#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "Common.hpp"

namespace CTR
{
    template<typename TConst, class TRet, class ... TArgs>
    struct PointerToFunctionTypeInfo : public CommonTypeInfo<TConst>
    {
        using Signature = TRet(TArgs...);
        using Type = CTR::ApplyConst<TConst, TRet(*)(TArgs ...)>;
    };

}//namespace CTR

#endif
