#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "Common.hpp"

#include <cstddef>//for std::size_t

namespace CTR
{
    template<typename TConst, typename TElementType, std::size_t TSize>
    struct ArrayTypeInfo : public CommonTypeInfo<TConst>
    {
        using ElementType = TElementType;
        using Type = TElementType[TSize];
    };

    template<typename TConst, typename TElementType>
    struct UnsizedArrayTypeInfo : public CommonTypeInfo<TConst>
    {
        using ElementType = TElementType;
        using Type = TElementType[];
    };

}//namespace CTR

#endif
