#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "../core/TypesAndConstants.hpp"

#include <type_traits>

namespace CTR { namespace Impl {
    namespace TypeCheckResult
    {
        namespace Value
        {
            namespace Plain
            {
                enum class EType : int
                {
                    Primitive,
                    Enum,
                    Union,
                    Class,
                    Unknown
                };

                template<class TType>
                constexpr static EType getType() {
                    return std::is_class<TType>::value ? EType::Class :
                           std::is_enum<TType>::value ? EType::Enum :
                           std::is_union<TType>::value ? EType::Union :
                           std::is_fundamental<TType>::value ? EType::Primitive :
                           EType::Unknown;
                }

                template<class TConst, class TType, EType TPlainValueType>
                struct SelectImpl;

                template<class TConst, class TType>
                struct SelectImpl<TConst, TType, EType::Primitive>
                {
                    using _ = PrimitiveTypeInfo<TConst, TType>;
                };

                template<class TConst, class TType>
                struct SelectImpl<TConst, TType, EType::Enum>
                {
                    using _ = EnumTypeInfo<TConst, TType>;
                };

                template<class TConst, class TType>
                struct SelectImpl<TConst, TType, EType::Union>
                {
                    using _ = UnionTypeInfo<TConst, TType>;
                };

                template<class TConst, class TType>
                struct SelectImpl<TConst, TType, EType::Class>
                {
                    using _ = ClassTypeInfo<TConst, TType>;
                };

                template<class TConst, class TType>
                using Select = typename SelectImpl<TConst, TType, getType<TType>()>::_;
            };

            template<class TConst, class TType>
            struct Impl
            {
                using _ = Plain::Select<TConst, TType>;
            };

            template<typename TConst, typename TElementType, std::size_t TSize>
            struct Impl<TConst, TElementType[TSize]>
            {
                using _ = ArrayTypeInfo<TConst, TElementType, TSize>;
            };

            template<typename TConst, typename TElementType>
            struct Impl<TConst, TElementType[]>
            {
                using _ = UnsizedArrayTypeInfo<TConst, TElementType>;
            };

            template<class TConst, class TClassType, class TMemberType>
            struct Impl<TConst, TMemberType TClassType::*>
            {
                using _ = ClassPointerToMemberTypeInfo<TConst, TClassType, TMemberType>;
            };

            template<class TConst, class TClassType, class TRet, class ... TArgs>
            struct Impl<TConst, TRet(TClassType::*)(TArgs ...)>
            {
                using _ = ClassPointerToMethodTypeInfo<TConst, TClassType, CTR::NonConst, TRet, TArgs ...>;
            };

            template<class TConst, class TClassType, class TRet, class ... TArgs>
            struct Impl<TConst, TRet(TClassType::*)(TArgs ...)const>
            {
                using _ = ClassPointerToMethodTypeInfo<TConst, TClassType, CTR::Const, TRet, TArgs ...>;
            };

            template<class TConst, class TRet, class ... TArgs>
            struct Impl<TConst, TRet(*)(TArgs ...)>
            {
                using _ = PointerToFunctionTypeInfo<TConst, TRet, TArgs ...>;
            };
        };

        // ================================================================================================================================
        //COMMON
        template<class TTypeToCheck, class TCheckingType>
        constexpr static bool isSame() {
            return std::is_same<TTypeToCheck, TCheckingType>::value;
        }

        template<class TTypeToCheck, class TCheckingType>
        constexpr static bool isSameOrAllowedByAny() {
            return isSame<TTypeToCheck, TCheckingType>() || isSame<TCheckingType, CTR::Any>();
        }

        // ================================================================================================================================

        //---------FAIL [DEFAULT]

        template<class TCheckingTypeInfo, class TCheckRequest>
        struct Impl{ using _ = CTR::Fail; };

        //---------PRIMITIVE

        template<
            class TCheckingTypeInfo_Const,
            class TCheckingTypeInfo_Type,
            class TCheckRequest_Const,
            class TCheckRequest_Type>
        struct Impl<
            PrimitiveTypeInfo<
                TCheckingTypeInfo_Const,
                TCheckingTypeInfo_Type>,
            PrimitiveTypeRequest<
                TCheckRequest_Const,
                TCheckRequest_Type>>
        {
            //TODO: Make fail details
            static constexpr bool _() {
                return
                    isSameOrAllowedByAny<TCheckingTypeInfo_Const, TCheckRequest_Const>() &&
                    isSameOrAllowedByAny<TCheckingTypeInfo_Type, TCheckRequest_Type>();
            }
        };

        //---------ENUM

        template<
            class TCheckingTypeInfo_Const,
            class TCheckingTypeInfo_Type,
            class TCheckRequest_Const,
            class TCheckRequest_Type>
        struct Impl<
            EnumTypeInfo<
                TCheckingTypeInfo_Const,
                TCheckingTypeInfo_Type>,
            EnumTypeRequest<
                TCheckRequest_Const,
                TCheckRequest_Type>>
        {
            //TODO: Make fail details
            static constexpr bool _() {
                return
                    isSameOrAllowedByAny<TCheckingTypeInfo_Const, TCheckRequest_Const>() &&
                    isSameOrAllowedByAny<TCheckingTypeInfo_Type, TCheckRequest_Type>();
            }
        };

        //---------UNION

        template<
            class TCheckingTypeInfo_Const,
            class TCheckingTypeInfo_Type,
            class TCheckRequest_Const,
            class TCheckRequest_Type>
        struct Impl<
            UnionTypeInfo<
                TCheckingTypeInfo_Const,
                TCheckingTypeInfo_Type>,
            UnionTypeRequest<
                TCheckRequest_Const,
                TCheckRequest_Type>>
        {
            //TODO: Make fail details
            static constexpr bool _() {
                return
                    isSameOrAllowedByAny<TCheckingTypeInfo_Const, TCheckRequest_Const>() &&
                    isSameOrAllowedByAny<TCheckingTypeInfo_Type, TCheckRequest_Type>();
            }
        };

        //---------CLASS

        template<
            class TCheckingTypeInfo_Const,
            class TCheckingTypeInfo_Type,
            class TCheckRequest_Const,
            class TCheckRequest_Type>
        struct Impl<
            ClassTypeInfo<
                TCheckingTypeInfo_Const,
                TCheckingTypeInfo_Type>,
            ClassTypeRequest<
                TCheckRequest_Const,
                TCheckRequest_Type>>
        {
            //TODO: Make fail details
            static constexpr bool _() {
                return
                    isSameOrAllowedByAny<TCheckingTypeInfo_Const, TCheckRequest_Const>() &&
                    isSameOrAllowedByAny<TCheckingTypeInfo_Type, TCheckRequest_Type>();
            }
        };

        //---------ARRAY

        template<
            class       TCheckingTypeInfo_Const,
            class       TCheckingTypeInfo_ElementType,
            std::size_t TCheckingTypeInfo_Size,
            class       TCheckRequest_Const,
            class       TCheckRequest_ElementType,
            std::size_t TCheckRequest_Size>
        struct Impl<
            ArrayTypeInfo<TCheckingTypeInfo_Const, TCheckingTypeInfo_ElementType, TCheckingTypeInfo_Size>,
            ArrayTypeRequest<TCheckRequest_Const, TCheckRequest_ElementType, TCheckRequest_Size>>
        {
            //TODO: Support size range & any size
            static constexpr bool _() {
                return
                    isSameOrAllowedByAny<TCheckingTypeInfo_Const, TCheckRequest_Const>() &&
                    isSameOrAllowedByAny<TCheckingTypeInfo_ElementType, TCheckRequest_ElementType>() &&
                    TCheckingTypeInfo_Size == TCheckRequest_Size;
            }
        };

        //---------UNSIZED ARRAY

        template<
            class TCheckingTypeInfo_Const,
            class TCheckingTypeInfo_ElementType,
            class TCheckRequest_Const,
            class TCheckRequest_ElementType>
        struct Impl<
            UnsizedArrayTypeInfo<
                TCheckingTypeInfo_Const,
                TCheckingTypeInfo_ElementType>,
            UnsizedArrayTypeRequest<
                TCheckRequest_Const,
                TCheckRequest_ElementType>>
        {
            //TODO: Maybe, merge with ARRAY impl
            static constexpr bool _() {
                return
                    isSameOrAllowedByAny<TCheckingTypeInfo_Const, TCheckRequest_Const>() &&
                    isSameOrAllowedByAny<TCheckingTypeInfo_ElementType, TCheckRequest_ElementType>();
            }
        };

        //---------MEMBER POINTER

        template<
            class TCheckingTypeInfo_Const,
            class TCheckingTypeInfo_MemberType,
            class TCheckingTypeInfo_ClassType,
            class TCheckRequest_Const,
            class TCheckRequest_MemberType,
            class TCheckRequest_ClassType>
        struct Impl<
            ClassMemberPointerTypeInfo<
                TCheckingTypeInfo_Const,
                TCheckingTypeInfo_MemberType,
                TCheckingTypeInfo_ClassType>,
            ClassMemberPointerTypeRequest<
                TCheckRequest_Const,
                TCheckRequest_MemberType,
                TCheckRequest_ClassType>>
        {
            //TODO: Make fail details
            static constexpr bool _() {
                return
                    isSameOrAllowedByAny<TCheckingTypeInfo_Const, TCheckRequest_Const>() &&
                    isSameOrAllowedByAny<TCheckingTypeInfo_MemberType, TCheckRequest_MemberType>() &&
                    isSameOrAllowedByAny<TCheckingTypeInfo_ClassType, TCheckRequest_ClassType>();
            }
        };

        //---------CLASS METHOD POINTER

        template<
            class TCheckingTypeInfo_Const,
            class TCheckingTypeInfo_ClassType,
            class TCheckingTypeInfo_MethodConst,
            class TCheckingTypeInfo_Ret,
            class ... TCheckingTypeInfo_Args,
            class TCheckRequest_Const,
            class TCheckRequest_ClassType,
            class TCheckRequest_MethodConst,
            class TCheckRequest_Ret,
            class ... TCheckRequest_Args>
        struct Impl<
            ClassPointerToMethodTypeInfo<
                TCheckingTypeInfo_Const,
                TCheckingTypeInfo_ClassType,
                TCheckingTypeInfo_MethodConst,
                TCheckingTypeInfo_Ret,
                TCheckingTypeInfo_Args...>,
            ClassPointerToMethodTypeRequest<
                TCheckRequest_Const,
                TCheckRequest_ClassType,
                TCheckRequest_MethodConst,
                TCheckRequest_Ret,
                TCheckRequest_Args...>>
        {
            //TODO: Make fail details
            static constexpr bool _() {
                return
                    isSameOrAllowedByAny<TCheckingTypeInfo_Const, TCheckRequest_Const>() &&
                    isSameOrAllowedByAny<TCheckingTypeInfo_ClassType, TCheckRequest_ClassType>() &&
                    isSameOrAllowedByAny<TCheckRequest_MethodConst, TCheckRequest_MethodConst>() &&
                    isSame<TCheckingTypeInfo_Ret(TCheckingTypeInfo_Args...), TCheckRequest_Ret(TCheckRequest_Args...)>();
            }
        };

        //--------FUNCTION POINTER

        template<
            class TCheckingTypeInfo_Const,
            class TCheckingTypeInfo_Ret,
            class ... TCheckingTypeInfo_Args,
            class TCheckRequest_Const,
            class TCheckRequest_Ret,
            class ... TCheckRequest_Args>
        struct Impl<
            FunctionPointerTypeInfo<
                TCheckingTypeInfo_Const,
                TCheckingTypeInfo_Ret,
                TCheckingTypeInfo_Args...>,
            FunctionPointerTypeRequest<
                TCheckRequest_Const,
                TCheckRequest_Ret,
                TCheckRequest_Args...>>
        {
            //TODO: Make fail details
            static constexpr bool _() {
                return
                    isSameOrAllowedByAny<TCheckingTypeInfo_Const, TCheckRequest_Const>() &&
                    isSame<TCheckingTypeInfo_Ret(TCheckingTypeInfo_Args...), TCheckRequest_Ret(TCheckRequest_Args...)>();
            }
        };

        //---------POINTER

        template<
            class TCheckingTypeInfo_Const,
            class TCheckingTypeInfo_PointedType,
            class TCheckRequest_Const,
            class TCheckRequest_PointedType>
        struct Impl<
            PointerTypeInfo<
                TCheckingTypeInfo_Const,
                TCheckingTypeInfo_PointedType>,
            PointerTypeRequest<
                TCheckRequest_Const,
                TCheckRequest_PointedType>>
        {
            //TODO: Make fail details
            static constexpr bool _() {
                return
                    isSameOrAllowedByAny<TCheckingTypeInfo_Const, TCheckRequest_Const>() &&
                    isSameOrAllowedByAny<TCheckingTypeInfo_PointedType, TCheckRequest_PointedType>();
            }
        };

        //---------REFERENCE

        template<
            class TCheckingTypeInfo_PointedType,
            class TCheckRequest_PointedType>
        struct Impl<
            ReferenceTypeInfo<
                TCheckingTypeInfo_PointedType>,
            ReferenceTypeRequest<
                TCheckRequest_PointedType>>
        {
            //TODO: Make fail details
            static constexpr bool _() {
                return
                    isSameOrAllowedByAny<TCheckingTypeInfo_PointedType, TCheckRequest_PointedType>();
            }
        };
    };

    // -------------------------------------

    namespace TypeCheckResult
    {
        template<class TTypeA, class TTypeB, bool TSelectTypeAIfTrueOrTypeBIfFalse>
        struct SelectTypeImpl;

        template<class TTypeA, class TTypeB>
        struct SelectTypeImpl<TTypeA, TTypeB, true> { using _ = TTypeA; };

        template<class TTypeA, class TTypeB>
        struct SelectTypeImpl<TTypeA, TTypeB, false> { using _ = TTypeB; };

        template<class TCheckingTypeInfo, class TCheckRequest>
        using Impl = SelectTypeImpl<
            PassedTypeCheckResult<TCheckingTypeInfo>,
            FailedTypeCheckResult<TCheckingTypeInfo>,
            CTR::Impl::CheckType::Impl<TCheckingTypeInfo, TCheckRequest>::_()>;
    }

}}//CTR::Impl

#endif
