#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "../core/TypesAndConstants.hpp"

namespace CTR{ namespace Impl{
	
   namespace IndirectionCleared{
        template<class Type>
        struct Impl { using _ = Type; };

        template<class TPointedType>
        struct Impl<TPointedType*> { using _ = typename Impl<TPointedType>::_; };

        template<class TPointedType>
        struct Impl<TPointedType* const> { using _ = typename Impl<TPointedType>::_; };

        template<class TPointedType>
        struct Impl<TPointedType&> { using _ = typename Impl<TPointedType>::_; };
   }

   namespace PointerToFunction
   {
        template<class TSignature>
        struct Impl { /*TODO: Add static assert*/ };

        template<class TRet, class ... TArgs>
        struct Impl<TRet(TArgs...)> { using _ = TRet(*)(TArgs...); };
   }

   namespace PointerToMethod
   {
        template<class TClass, class TSignature, class TMethodConst>
        struct Impl { /*TODO: Add static assert*/ };

        template<class TClass, class TRet, class ... TArgs>
        struct Impl<TClass, TRet(TArgs...), CTR::NonConst> { using _ = TRet(TClass::*)(TArgs...); };

        template<class TClass, class TRet, class ... TArgs>
        struct Impl<TClass, TRet(TArgs...), CTR::Const> { using _ = TRet(TClass::*)(TArgs...)const; };
   }
   
   namespace ApplyConst
   {
       template<class TConst, class TType>
       struct Impl;

       template<class TType>
       struct Impl<NonConst, TType>
       {
           using _ = TType;
       };

       template<class TType>
       struct Impl<Const, TType>
       {
           using _ = const TType;
       };
   }   
}}

#endif
