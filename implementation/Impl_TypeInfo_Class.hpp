#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "../core/TypesAndConstants.hpp"
#include "../typeGen/Pointers.hpp"//for all class member pointer types

#include <utility>//For std::declval

namespace CTR{ namespace Impl{ namespace ClassTypeInfoDetails{

    namespace GetPointerToField
    {
        template<class TClassType, class TMeta, class TFieldType, class ... TFieldSpecializationArgs>
        constexpr static auto _() {
            using ResPointerToFieldType = CTR::PointerToMember<TFieldType, TClassType>;
            return TMeta::template getPointerToMember<
                    TClassType, ResPointerToFieldType, ResPointerToFieldType, TFieldSpecializationArgs...>(nullptr);
        }
    }

    namespace GetObjectFieldPointer
    {
        template<class TClassType, class TMeta, class TFieldType, class ... TFieldSpecializationArgs>
        static const TFieldType* _(const TClassType& inObject) {
            auto thePointerToField = TMeta::template getPointerToMember<
                    TClassType,
                    CTR::PointerToMember<TFieldType, TClassType>,
                    TFieldSpecializationArgs...>(nullptr);
            return (thePointerToField != nullptr) ? inObject.*thePointerToField : nullptr;
        }
    }

    namespace GetMethod
    {
        template<class TRequestConst, class ... TSpecArgs> struct ParsedMiscArgs;

        namespace ImplParsedMiscArgs
        {
            namespace ImplFinal
            {
                template<class TClassType, class TMeta, class TSignature, class TSelectedConst, class ...TSpecArgs>
                struct Res
                {
                    constexpr static auto _() {
                        using ResPointerToMethodType = CTR::PointerToMethod<TClassType, TSignature, TSelectedConst>;
                        return TMeta::template getPointerToMember<
                                TClassType, ResPointerToMethodType, ResPointerToMethodType, TSpecArgs...>(nullptr);
                    }
                };

                template<class TClassType, class TMeta, class TSignature, class ...TSpecArgs>
                struct Res<TClassType, TMeta, TSignature, CTR::Any, TSpecArgs...>
                {
                private:
                    using ResPointerToNonConstMethodType = CTR::PointerToMethod<TClassType, TSignature, CTR::NonConst>;
                    using ResPointerToConstMethodType = CTR::PointerToMethod<TClassType, TSignature, CTR::Const>;

                    constexpr static auto resSelection(ResPointerToNonConstMethodType inSuccessFoundNonConstMethod) {
                        return inSuccessFoundNonConstMethod;
                    }

                    constexpr static auto resSelection(CTR::Fail*) {
                        return TMeta::template getPointerToMember<
                                TClassType, ResPointerToConstMethodType, CTR::Fail*, TSpecArgs...>(nullptr);
                    }

                public:
                    constexpr static auto _() {
                        return TMeta::template getPointerToMember<
                                TClassType, ResPointerToNonConstMethodType, CTR::Fail*, TSpecArgs...>(nullptr);
                    }
                };
            }

            template<class TClassConst, class TRequestMethodConst>
            struct MethodRequestConst { using _ = TRequestMethodConst; };

            template<class TClassConst>
            struct MethodRequestConst<TClassConst, CTR::None> { using _ = TClassConst; };

            template<class TClassConst, class TClassType, class TMeta, class TSignature, class TParsedMiscArgs>
            struct Res;

            template<class TClassConst, class TClassType, class TMeta, class TSignature, class TRequestConst, class ... TSpecArgs>
            struct Res<TClassConst, TClassType, TMeta, TSignature, ParsedMiscArgs<TRequestConst, TSpecArgs...>>
            {
                using _ = ImplFinal::Res<TClassType,
                    TMeta, TSignature, typename MethodRequestConst<TClassConst, TRequestConst>::_, TSpecArgs...>;
            };
        }

        namespace ParseMiscArgs
        {
            template<class ... TSpecArgs> struct Res { using _ = ParsedMiscArgs<CTR::None, TSpecArgs...>; };
            template<class ... TSpecArgs> struct Res<CTR::Const, TSpecArgs...> { using _ = ParsedMiscArgs<CTR::Const, TSpecArgs...>; };
            template<class ... TSpecArgs> struct Res<CTR::NonConst, TSpecArgs...> { using _ = ParsedMiscArgs<CTR::NonConst, TSpecArgs...>; };
            template<class ... TSpecArgs> struct Res<CTR::Any, TSpecArgs...> { using _ = ParsedMiscArgs<CTR::Any, TSpecArgs...>; };
        }

        template<class TConst, class TClassType, class TMeta, class TSignature, class ... TOptionalArgs>
        using Res = typename ImplParsedMiscArgs::Res<
            TConst,
            TClassType,
            TMeta,
            TSignature,
            typename ParseMiscArgs::Res<TOptionalArgs...>::_>::_;
    }

    //TODO: Implement
//    template<class TConst, class TClassType>
//    template<class TMeta, class ... TOptionalArgs>
//    constexpr bool ClassTypeInfo<TConst, TClassType>::isMethodMayBeCalled() {

    namespace IsConstructorMayBeCalled
    {
        template<class TClassType, class ... TArgumentToPassTypes>
        constexpr static bool _() {
            return std::is_constructible<TClassType, TArgumentToPassTypes ...>::value;
        }
    }

    namespace IsDefaultConstructorMayBeCalled
    {
        template<class TClassType>
        constexpr static bool _() {
            return std::is_default_constructible<TClassType>::value;
        }
    }

    namespace IsDestructorMayBeCalled
    {
        template<class TClassType>
        constexpr bool _() {
            return std::is_destructible<TClassType>::value;
        }
    }

//STATIC
//    constexpr TFieldType* ClassTypeInfo<TConst, TClassType>::Static::getFieldPointer() {
//    constexpr CTR::PointerToFunction<TSignature> ClassTypeInfo<TConst, TClassType>::Static::getMethodPointer() {

}}}//CTR::Impl::ClassTypeInfoDetails

#endif
