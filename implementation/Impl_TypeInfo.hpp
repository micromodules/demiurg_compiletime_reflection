#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "../typeInfo/Array.hpp"
#include "../typeInfo/Class.hpp"
#include "../typeInfo/Enum.hpp"
#include "../typeInfo/Pointer.hpp"
#include "../typeInfo/PointerToFunction.hpp"
#include "../typeInfo/PointerToMember.hpp"
#include "../typeInfo/PointerToMethod.hpp"
#include "../typeInfo/Primitive.hpp"
#include "../typeInfo/Reference.hpp"
#include "../typeInfo/Union.hpp"

namespace CTR { namespace Impl {
    namespace TypeInfo
    {
        struct Value
        {
            struct Plain
            {
                enum class EType : int
                {
                    Primitive,
                    Enum,
                    Union,
                    Class,
                    Unknown
                };

                template<class TType>
                constexpr static EType getType() {
                    return std::is_class<TType>::value ? EType::Class :
                           std::is_enum<TType>::value ? EType::Enum :
                           std::is_union<TType>::value ? EType::Union :
                           std::is_fundamental<TType>::value ? EType::Primitive :
                           EType::Unknown;
                }

                template<class TConst, class TType, EType TPlainValueType>
                struct SelectImpl;

                template<class TConst, class TType>
                struct SelectImpl<TConst, TType, EType::Primitive>
                {
                    using _ = PrimitiveTypeInfo<TConst, TType>;
                };

                template<class TConst, class TType>
                struct SelectImpl<TConst, TType, EType::Enum>
                {
                    using _ = EnumTypeInfo<TConst, TType>;
                };

                template<class TConst, class TType>
                struct SelectImpl<TConst, TType, EType::Union>
                {
                    using _ = UnionTypeInfo<TConst, TType>;
                };

                template<class TConst, class TType>
                struct SelectImpl<TConst, TType, EType::Class>
                {
                    using _ = ClassTypeInfo<TConst, TType>;
                };

                template<class TConst, class TType>
                using Select = typename SelectImpl<TConst, TType, getType<TType>()>::_;
            };

            template<class TConst, class TType>
            struct Impl
            {
                using _ = Plain::Select<TConst, TType>;
            };

            template<typename TConst, typename TElementType, std::size_t TSize>
            struct Impl<TConst, TElementType[TSize]>
            {
                using _ = ArrayTypeInfo<TConst, TElementType, TSize>;
            };

            template<typename TConst, typename TElementType>
            struct Impl<TConst, TElementType[]>
            {
                using _ = UnsizedArrayTypeInfo<TConst, TElementType>;
            };

            template<class TConst, class TClassType, class TMemberType>
            struct Impl<TConst, TMemberType TClassType::*>
            {
                using _ = ClassPointerToMemberTypeInfo<TConst, TClassType, TMemberType>;
            };

            template<class TConst, class TClassType, class TRet, class ... TArgs>
            struct Impl<TConst, TRet(TClassType::*)(TArgs ...)>
            {
                using _ = ClassPointerToMethodTypeInfo<TConst, TClassType, CTR::NonConst, TRet, TArgs ...>;
            };

            template<class TConst, class TClassType, class TRet, class ... TArgs>
            struct Impl<TConst, TRet(TClassType::*)(TArgs ...)const>
            {
                using _ = ClassPointerToMethodTypeInfo<TConst, TClassType, CTR::Const, TRet, TArgs ...>;
            };

            template<class TConst, class TRet, class ... TArgs>
            struct Impl<TConst, TRet(*)(TArgs ...)>
            {
                using _ = PointerToFunctionTypeInfo<TConst, TRet, TArgs ...>;
            };
        };

        template<class TType>
        struct Impl{ using _ = typename Value::Impl<NonConst, TType>::_; };

        template<class TType>
        struct Impl<const TType>{ using _ = typename Value::Impl<Const, TType>::_; };

        template<class TType>
        struct Impl<TType*>{ using _ = PointerTypeInfo<NonConst, TType>; };

        template<class TType>
        struct Impl<TType* const>{ using _ = PointerTypeInfo<Const, TType>; };

        template<class TType>
        struct Impl<TType&>{ using _ = ReferenceTypeInfo<TType>; };
    };
}}//CTR::Impl

#endif
