#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "../implementation/Impl_TypeGens.hpp"

namespace CTR
{
    template<typename Type>
    using IndirectionCleared = typename CTR::Impl::IndirectionCleared::Impl<Type>::_;
}

#endif
