#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "ClearingIndirection.hpp"
#include "../implementation/Impl_TypeGens.hpp"

namespace CTR
{	
    //TODO: Move to separate file
    template<typename TSignature>
    using PointerToFunction = typename CTR::Impl::PointerToFunction::Impl<
        TSignature>::_;

    //TODO: Move to separate file
    template<typename TMemberType, typename TClassType>
    using PointerToMember = TMemberType IndirectionCleared<TClassType>::*;

    template<typename TClass, typename TSignature, class TMethodConst>
    using PointerToMethod = typename CTR::Impl::PointerToMethod::Impl<
        IndirectionCleared<TClass>, TSignature, TMethodConst>::_;
}

#endif
