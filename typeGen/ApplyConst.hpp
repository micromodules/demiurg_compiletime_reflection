#pragma once
#include "_Module.hpp"
#ifdef MODULE__demiurg_compiletime_reflection__Version

#include "../implementation/Impl_TypeGens.hpp"
#include "../core/TypesAndConstants.hpp"//ApplyConst is used with CTR::Const

namespace CTR
{
    //TODO: Move ApplyConst to common?
    template<class TConst, class TType>
    using ApplyConst = typename Impl::ApplyConst::Impl<TConst, TType>::_;

}//namespace CTR::Impl

// ========================================================================================================================================

#endif
